# xbuild-nyarla

software building tools like [tagomoris's xbuild](https://github.com/tagomoris/xbuild)

# Supported softwares

* nginx: `nginx-install`
* apache: `apache-install`
* znc: `znc-install`


# How to use

## `nginx-install`

    $ nginx-install 1.4.0 ~/local/nginx

## `apache-install`

    $ apache-install 2.4.4 ~/local/apache

## `znc-install`

    $ znc-install 1.0 ~/local/znc

# License

* `nginx-install`: MIT-license
* `apache-install`: MIT-license
* `znc-install`: MIT-license

# Author

Naoki OKAMURA (Nyarla) <nyarla@thotep.net>

